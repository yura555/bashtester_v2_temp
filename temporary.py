import docker

client = docker.from_env()
print(client.info()['Swarm']['LocalNodeState'])


client.services.create(
    image='bashtester/ubuntu:latest',
    name='bashtester_managed_user',
    hostname='bashtester_managed_user',
    networks=["bashtester_bashtester_net"],
    tty=True,
    resources={'mem_limit': '512m', 'cpu_limit': '1'}

)
service = client.services.get('bashtester_managed_user')

# service.remove()
