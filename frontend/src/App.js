import React, { Component } from "react";
import { withTranslation } from 'react-i18next';
import { BrowserRouter as Router, Routes, Route}
    from 'react-router-dom';
import AdminMain from "./components/AdminPage";
// import UploadForm from "./components/AdminPage";

import "./App.css";
import Typed from "typed.js";
import { getOSList, removeContainerById, testContainerById } from "./util/api";
import LoadingScreen from "react-loading-screen";

import { getItem, rmItem } from "./util/util";
import { 
  Typography,
  Button, 
  Tooltip, 
  Divider, 
  Card, 
  Modal, 
  List, 
  Input, 
  Alert} from "antd";

import SelectSystemConfig from "./components/SelectSystemConfig";
import SystemConfiguration from "./components/SystemConfiguration";
import LanguageSwitcher from "./components/LanguageSwitcher";

const {Text} = Typography;

class App extends Component {
  constructor(props) {
    super(props);
    this.t = props.t;

    const { isExistContainer, container } = this.isExistContainer();

    this.state = {
      username: "user",
      isAdminPage: false,
      isAdmin: true,
      testName: "",
      testInfo: [],
      isTestInfo: false,
      errorTestRun: false,
      container,
      isExistContainer,
      screenLoading: false,
      skipModalVisible: false
    };
  }

  componentDidMount = () => {
    this.subscribeEvent();

    if (document.getElementsByClassName('app__desc-content').length > 0) {
      // this.setState({errorTestRun: false});
      this.typed = new Typed(".app__desc-content", {
        strings: [
          this.t('site.typed')
        ],
        typeSpeed: 10
      });
    }
  };

  componentWillUnmount = () => {};

  // Проверка существования контейнера
  isExistContainer = () => {
    // Получение информации из локального хранилища
    let containerInfo = getItem("containerInfo");
    // Информации есть => контейнер существует
    if (containerInfo) {
      containerInfo = JSON.parse(containerInfo);
      const curTime = Math.floor(new Date().getTime() / 1000);
      // Если время работы истекло
      if (curTime < containerInfo.timeout) {
        return { isExistContainer: true, container: containerInfo };
      } 
      else {
        rmItem("containerInfo");
      }
    }
    return { isExistContainer: false, container: {} }; 
  };

  subscribeEvent = () => {};

  handleSelectAgain = async () => {
    this.setState({ screenLoading: true, screenText: this.t('prompt.purging') });
    const { container } = this.state;
    const timestamp = Math.floor(new Date().getTime() / 1000);
    this.p3 = removeContainerById(
      container.containerId,
      container.shareUrl,
      timestamp
    );
    let res;
    try {
      res = await this.p3.promise;
    } catch (err) {
      return console.error(err);
    }
    if (res.statusCode !== 1) {
      this.setState({ screenLoading: false });
      return console.error(res.message);
    }
    this.setState({
      testInfo: [],
      isExistContainer: false,
      container: {},
      screenLoading: false
    });
    rmItem("containerInfo");
  };

  runTest = async () => {
    this.setState({isTestInfo: true, testInfo: []});
    const { container, testName } = this.state;
    this.p4 = testContainerById(container.containerId, testName);
    let res;
    try {
      res = await this.p4.promise;
    } catch (err) {
      this.setState({isTestInfo: false});
      return console.error(res.message);
    }
    if (res.statusCode !== 1) {
      this.setState({errorTestRun: true, isTestInfo: false});
      return console.error(res.message);
    } 
    else {
      this.setState({testInfo: res.tests, isTestInfo: false});
    }
  };

  handleOkCallback = () => {
    const { isExistContainer, container } = this.isExistContainer();
    this.setState({ isExistContainer, container, skipModalVisible: true });
  };

  handleInputChange = (event) => {
    this.setState({ testName: event.target.value, 
                    errorTestRun: false});
  };

  render() {
    const {
      username,
      isAdmin,
      isAdminPage,
      testInfo,
      isTestInfo,
      errorTestRun,
      isExistContainer,
      screenLoading,
      screenText
    } = this.state;
    return (
      <div>
        {isAdminPage ? 
          (
            <div>
              <div className="app__button">
                <Button     
                    size="large"
                    type="primary"
                    variant="outlined"
                    onClick={() => {
                      this.setState({isAdminPage: false});
                    }}
                  >
                  {this.t('keyword.back')} 
                  </Button>
              </div>
              <div>
                <AdminMain/>
              </div>
            </div>
          ) : (
            <LoadingScreen
              loading={screenLoading}
              bgColor="#d0d0d0"
              spinnerColor="#252525"
              textColor="#676767"
              text={screenText}
            >
              <div className="app">
                <h1 className="app__title">
                  <span className="app__title-span">
                    BashTester
                  </span>
                </h1>

                {/* <LanguageSwitcher className="app__lang-switcher" i18n={this.props.i18n} /> */}

                {isAdmin && (
                    <div className="app__button">
                      <Button     
                        size="large"
                        type="primary"
                        variant="outlined"
                        onClick={() => {
                          this.setState({isAdminPage: true});
                        }}
                      >
                        {this.t('prompt.admin')} 
                      </Button>
                    </div>
                  )
                }

                <Divider style={{ marginTop: 100 }}>
                  {isExistContainer ? this.t('prompt.testing-title') : this.t('prompt.run-system-title')}
                </Divider>

                {isExistContainer && testInfo.length > 0 && (
                  <List 
                  bordered
                  dataSource={testInfo}
                  renderItem={(item) => (
                    <List.Item>
                      {item.result === "прошел" ?
                        <Text type="success"> {item.name} : {item.result} </Text> :
                        <Text type="danger"> {item.name} : {item.result} </Text> 
                      }
                    </List.Item>
                  )}/>
                )}

                <div className="app__os-list">
                  {isExistContainer ? (
                    <div style={{ marginTop: 20, textAlign: "center" }}>
                      <Tooltip>

                        {errorTestRun && (
                            <Alert 
                              message={this.t('sentence.err-msg-testname')}
                              type="error" showIcon banner closable 
                            />
                          )
                        }

                        <Input
                          placeholder={this.t('prompt.enter-testname')}
                          onChange={this.handleInputChange}
                        />
                        
                        <Button
                          loading={isTestInfo ? true : false}
                          size="large"
                          type="primary"
                          variant="outlined"
                          onClick={this.runTest}
                          style={{ margin: 10 }}
                        >
                        {this.t('prompt.run-testing')} 
                        </Button>
                          
                        <Button
                          size="large"
                          color="green"
                          variant="outlined"
                          onClick={() => {
                            const containerInfo = JSON.parse(
                              getItem("containerInfo")
                            );
                            window.open(containerInfo.shareUrl.replace('http://:', `http://${window.location.hostname}:`));
                          }}
                          style={{ margin: 10 }}
                        >
                          {this.t('prompt.open-os')}
                        </Button>
                      </Tooltip>

                      <Button
                        size="large"
                        type="primary"
                        danger
                        color="secondary"
                        variant="outlined"
                        onClick={this.handleSelectAgain}
                        style={{ margin: 10 }}
                      >
                        {this.t('prompt.purge-os')}
                      </Button>
                    </div>
                  ) : (
                    <SelectSystemConfig username={username} okCallback={this.handleOkCallback} />
                  )}
                </div>
              </div>
              <Modal
                title={this.t('keyword.notice')}
                visible={this.state.skipModalVisible}
                onOk={() => {
                  window.open(this.state.container.shareUrl.replace('http://:', `http://${window.location.hostname}:`));
                  this.setState({ skipModalVisible: false });
                }}
                okText={this.t('keyword.ok')}
                cancelText={this.t('keyword.cancel')}
                onCancel={() => this.setState({ skipModalVisible: false })}
              > 
                <p>{this.t('sentence.open-webshell')}</p>
              </Modal>
          </LoadingScreen>)
        }
      </div>
    ); 
  }
}

export default withTranslation()(App);

// Для запуска с возможностью выбора ресурсов
// class App extends Component {
//   constructor(props) {
//     super(props);
//     this.t = props.t;

//     const { isExistContainer, container } = this.isExistContainer();

//     this.state = {
//       open: false,
//       osList: [],
//       testName: "",
//       testInfo: [],
//       isTestInfo: false,
//       errorTestRun: false,
//       selectedVersion: {},
//       selectedOS: {},
//       timeout: 24,
//       cpu: 1,
//       memory: 512,
//       port: 80, // Internal port (entered by user)
//       externalPort: 0, // External port (assigned by api)
//       container,
//       isExistContainer,
//       screenLoading: false,
//       skipModalVisible: false
//     };
//   }

//   componentDidMount = () => {
//     this.getOSList();
//     this.subscribeEvent();

//     if (document.getElementsByClassName('app__desc-content').length > 0) {
//       // this.setState({errorTestRun: false});
//       this.typed = new Typed(".app__desc-content", {
//         strings: [
//           'echo "BashTester"',
//           // this.t('site.typed')
//         ],
//         typeSpeed: 10
//       });
//     }
//   };

//   componentWillUnmount = () => {};

//   isExistContainer = () => {
//     let containerInfo = getItem("containerInfo");
//     if (containerInfo) {
//       containerInfo = JSON.parse(containerInfo);
//       const curTime = Math.floor(new Date().getTime() / 1000);
//       // Check if it's still valid
//       if (curTime < containerInfo.timeout) {
//         return { isExistContainer: true, container: containerInfo };
//       } else {
//         rmItem("containerInfo");
//       }
//     }
//     return { isExistContainer: false, container: {} }; 
//   };

//   subscribeEvent = () => {};

//   getOSList = async () => {
//     this.p1 = getOSList();
//     let res;
//     try {
//       res = await this.p1.promise;
//     } catch (err) {
//       return console.error(err);
//     }
//     const osList = res;
//     if (osList.length) {
//       osList[0].selected = true;
//     }
//     this.setState({ osList });
//   };

//   handleOSSelect = selectedOS => {
//     const osList = [...this.state.osList];

//     const i = osList.findIndex(os => selectedOS.label === os.label);

//     osList.forEach((os, j) => {
//       if (i === j) {
//         os.selected = false;
//       }
//       os.selected = i === j;
//     });
//     this.setState({ osList });
//   };

//   handleOSVersionSelect = (selectedOS, selectedVersion) => {
//     this.setState({
//       open: true,
//       selectedOS,
//       selectedVersion
//     });
//   };

//   handleSelectAgain = async () => {
//     this.setState({ screenLoading: true, screenText: this.t('prompt.purging') });
//     const { container } = this.state;
//     const timestamp = Math.floor(new Date().getTime() / 1000);
//     this.p3 = removeContainerById(
//       container.containerId,
//       container.shareUrl,
//       timestamp
//     );
//     let res;
//     try {
//       res = await this.p3.promise;
//     } catch (err) {
//       return console.error(err);
//     }
//     if (res.statusCode !== 1) {
//       this.setState({ screenLoading: false });
//       return console.error(res.message);
//     }
//     this.setState({
//       testInfo: [],
//       isExistContainer: false,
//       container: {},
//       screenLoading: false
//     });
//     rmItem("containerInfo");
//   };

//   runTest = async () => {
//     this.setState({isTestInfo: true, testInfo: []});
//     const { container, testName } = this.state;
//     this.p4 = testContainerById(container.containerId, testName);
//     let res;
//     try {
//       res = await this.p4.promise;
//     } catch (err) {
//       return console.error(res.message);
//     }
//     if (res.statusCode !== 1) {
//       this.setState({errorTestRun: true, isTestInfo: false});
//     } 
//     else {
//       this.setState({testInfo: res.tests, isTestInfo: false});
//     }
//   };

//   handleOkCallback = () => {
//     const { isExistContainer, container } = this.isExistContainer();
//     this.setState({ isExistContainer, container, skipModalVisible: true });
//   };

//   handleInputChange = (event) => {
//     this.setState({ testName: event.target.value, 
//                     errorTestRun: false});
//   };

//   render() {
//     const {
//       testInfo,
//       isTestInfo,
//       errorTestRun,
//       isExistContainer,
//       screenLoading,
//       screenText,
//       container
//     } = this.state;
//     return (
//       <LoadingScreen
//         loading={screenLoading}
//         bgColor="#d0d0d0"
//         spinnerColor="#252525"
//         textColor="#676767"
//         text={screenText}
//       >
//         <div className="app">
//           <h1 className="app__title">
//             <span className="app__title-span">
//               BashTester
//             </span>
//           </h1>
//           {/* {!isExistContainer && (<div className="app__desc">
//             <div className="app__text-editor-wrap">
//               <div className="app__title-bar">
//                 Bash
//               </div>
//               <div className="app__text-body">
//                 <span style={{ marginRight: 10 }}>$</span>
//                 <span className="app__desc-content" />
//               </div>
//             </div>
//           </div>)} */}
//           {/* <LanguageSwitcher className="app__lang-switcher" i18n={this.props.i18n} /> */}

//           <Divider style={{ marginTop: 100 }}>
//             {isExistContainer ? "Тестирование" : "Выберите систему"/*this.t('prompt.select-os')*/}
//             {/* {isExistContainer ? this.t('prompt.created-os') : this.t('prompt.select-os')} */}
//           </Divider>

//           {isExistContainer && (
//             <List 
//             bordered
//             dataSource={testInfo}
//             renderItem={(item) => (
//               <List.Item>
//                 {item.result === "passing" ?
//                   <span style={{ color: "green" }}>{item.name} : {item.result}</span> :
//                   <span style={{ color: "red" }}>{item.name} : {item.result}</span>
//                 }
//               </List.Item>
//             )}/>
//             // <Card>
//             //   <div className="app__ports">
//             //     <SystemConfiguration
//             //       showInnerPort
//             //       showExternalPort
//             //       system={container.system}
//             //       version={container.version}
//             //       cpu={container.cpu}
//             //       mem={container.mem}
//             //       timeout={container.timeoutH}
//             //       innerPort={container.innerPort}
//             //       externalPort={container.externalPort}
//             //     />
//             //   </div>
//             // </Card>
//           )}

//           <div className="app__os-list">
//             {isExistContainer ? (
//               <div style={{ marginTop: 20, textAlign: "center" }}>
//                 <Tooltip title={this.t('sentence.open-webshell-try-again')}>

//                     {errorTestRun && (<Alert 
//                                         message="Задания с таким названием не существует" 
//                                         type="error" showIcon banner closable />)}

//                     <Input
//                       placeholder="Введите название задания"
//                       onChange={this.handleInputChange}
//                     />
//                     <Button
//                       loading={isTestInfo ? true : false}
//                       size="large"
//                       color="primary"
//                       variant="outlined"
//                       onClick={this.runTest}
//                       style={{ margin: 10 }}
//                     >
//                     Запустить тестирование
//                     </Button>
                    
//                   <Button
//                     size="large"
//                     color="green"
//                     variant="outlined"
//                     onClick={() => {
//                       const containerInfo = JSON.parse(
//                         getItem("containerInfo")
//                       );
//                       window.open(containerInfo.shareUrl.replace('http://:', `http://${window.location.hostname}:`));
//                     }}
//                     style={{ margin: 10 }}
//                   >
//                     Открыть терминал
//                     {/* {this.t('prompt.open-os')} */}
//                   </Button>
//                 </Tooltip>

//                 <Button
//                   size="large"
//                   type="danger"
//                   color="secondary"
//                   variant="outlined"
//                   onClick={this.handleSelectAgain}
//                   style={{ margin: 10 }}
//                 >
//                   Завершить работу
//                   {/* {this.t('prompt.purge-os')} */}
//                 </Button>
//               </div>
//             ) : (
//               <SelectSystemConfig osList={this.state.osList} okCallback={this.handleOkCallback} />
//             )}
//           </div>
//         </div>
//         <Modal
//           title="Уведомление"
//           // title={this.t('keyword.notice')}
//           visible={this.state.skipModalVisible}
//           onOk={() => {
//             window.open(this.state.container.shareUrl.replace('http://:', `http://${window.location.hostname}:`));
//             this.setState({ skipModalVisible: false });
//           }}
//           okText="Продолжить"
//           cancelText="Отменить"
//           // okText={this.t('keyword.ok')}
//           // cancelText={this.t('keyword.cancel')}
//           onCancel={() => this.setState({ skipModalVisible: false })}
//         > 
//           <p>Система создана. Запустить терминал?</p>
//           {/* <p>{this.t('sentence.open-webshell')}</p> */}
//         </Modal>
//       </LoadingScreen>
//     );
//   }
// }

// export default withTranslation()(App);
