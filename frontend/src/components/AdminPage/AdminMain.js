import React, {useState} from "react";
import { withTranslation } from 'react-i18next';
import {
  Menu,
  Button
} from "antd";
import { ToTopOutlined, BarChartOutlined, SettingFilled } from '@ant-design/icons'
import UploadForm from "./UploadForm"
import ConfigForm from "./ConfigForm"


function AdminMain(props) {

    const [current, setCurrent] = useState('upload');
    const onClick = (e) => {
      console.log('click ', e);
      setCurrent(e.key);
    };

    const items = [
      {
        label: 'Результаты',
        key: 'result',
        icon: <BarChartOutlined/>,
        disabled: false,
      },
      {
        label: 'Загрузка файлов',
        key: 'upload',
        icon: <ToTopOutlined/>,
      },
      {
        label: 'Конфигурация контейнеров',
        key: 'config',
        icon: <SettingFilled />,
      },
    ];

    return (
      <div>
          <Menu onClick={onClick} selectedKeys={[current]} mode={"horizontal"} items={items} />
          {current === "upload" && <UploadForm/>}
          {current === "config" && <ConfigForm/>}
      </div>

    
    );
}

export default AdminMain;
