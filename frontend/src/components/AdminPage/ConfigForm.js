import React from "react";
import { withTranslation } from 'react-i18next';
import {
  Input,
  Form,
  Button,
  Space
} from "antd";

import {setConfig} from "../../util/api"
import "../SelectSystemConfig/SelectSystemConfig.css"
const FormItem = Form.Item;

/**
 * A form for instantbox requests
 */
class ConfigForm extends React.Component {
  // static propTypes = {};
  // static defaultProps = {};

  constructor(props) {
    super(props);
    this.t = props.t;
    
    this.state = {
      isSet: false,
      os: "",
      cpu: "",
      memory: "",
      timeout: ""
    };
  }

  getRules = () => {
    return {
      port: [
        {
          message: this.t('prompt.enter-port')
        },
        {
          validator: (rule, value, callback) => {
            if (
              (/^\d+$/g.test(value) && value >= 1 && value <= 65535) ||
              value === ""
            ) {
              return callback();
            }
            callback(this.t('sentence.err-port'));
          },
          message: this.t('sentence.msg-port')
        }
      ],
      cpu: [
        {
          message: this.t('prompt.enter-cpu-core-count')
        },
        {
          validator: (rule, value, callback) => {
            if (
              (/^\d+$/g.test(value) && value >= 1 && value <= 4) ||
              value === ""
            ) {
              return callback();
            }
            callback(this.t('sentence.err-cpu-core-count'));
          },
          message: this.t('sentence.msg-cpu-core-count')
        }
      ],
      memory: [
        {
          message: this.t('prompt.enter-memory-in-mb')
        },
        {
          validator: (rule, value, callback) => {
            if (
              (/^\d+$/g.test(value) && value >= 1 && value <= 3584) ||
              value === ""
            ) {
              return callback();
            }
            callback(this.t('sentence.err-memory-in-mb'));
          },
          message: this.t('sentence.msg-memory-in-mb')
        }
      ],
      timeout: [
        {
          message: this.t('prompt.enter-ttl-in-hours')
        },
        {
          validator: (rule, value, callback) => {
            if (
              (/^\d+$/g.test(value) && value >= 1 && value <= 24) ||
              value === ""
            ) {
              return callback();
            }
            callback(this.t('sentence.err-ttl-in-hours'));
          },
          message: this.t('sentence.msg-ttl-in-hours')
        }
      ]
    }
  }

  handleInputChange = (event, paramName) => {
    this.setState({ [paramName]: event.target.value });
  };

  changeConfig = async (param) => {
    this.setState({isSet: true});
    const value = this.state[param];
    this.p = setConfig(param, value);
    let res;
    try {
      res = await this.p.promise;
    } catch (err) {
      this.setState({isSet: false});
      return console.error(res.message);
    }
    this.setState({isSet: false});
  }

  render() {
    const {
      isSet
    } = this.state;
    const rules = this.getRules()
    const change = "Изменить"
    return (
      <div className="select-system-config" style={{"minHeight": "200px",
                                                    "width": "1000px", 
                                                    "paddingBottom": "0px"}}>
        <Form 
          layout="horizontal" 
          labelCol={{ span: 10 }}
          wrapperCol={{ span: 14 }}
        >
          <Form.Item 
            label="Название образа"
          >
            <Space.Compact>
              <Input
                defaultValue="bashtester/ubuntu:latest" 
                style={{ width: 300 }} 
                placeholder="напр. bashtester/ubuntu:latest"
                onChange={(e) => this.handleInputChange(e, "os")}
              />
              <Button
                loading={isSet}
                size="large"
                type="primary"
                variant="outlined"
                onClick={() => this.changeConfig("os")}
              >
                {change}
              </Button>
            </Space.Compact>
          </Form.Item>

          <Form.Item 
            label="Ядра ЦП"
            name="cpu" 
            rules={rules.cpu}
          >
            <Space.Compact>
              <Input 
                defaultValue="1"
                style={{ width: 300 }} 
                placeholder={this.t('sentence.eg-cpu-core-count')}
                onChange={(e) => this.handleInputChange(e, "cpu")} 
              />
              <Button
                loading={isSet}
                size="large"
                type="primary"
                variant="outlined"
                onClick={() => this.changeConfig("cpu")}
              >
                {change}
              </Button>
            </Space.Compact>
          </Form.Item>

          <Form.Item 
            label="Память (МБ)"
            name="memory"
            rules={rules.memory}
          >
            <Space.Compact>
              <Input 
                defaultValue="512"
                style={{ width: 300 }} 
                placeholder={this.t('sentence.eg-memory-in-mb')}
                onChange={(e) => this.handleInputChange(e, "memory")}
              />
              <Button
                loading={isSet}
                size="large"
                type="primary"
                variant="outlined"
                onClick={() => this.changeConfig("memory")}
              >
                {change} 
              </Button>
            </Space.Compact>
          </Form.Item>

          <Form.Item 
            label="Время (Ч)"
            name="timeout"
            rules={rules.timeout}
          >
            <Space.Compact>
              <Input 
                defaultValue="24"
                style={{ width: 300 }} 
                placeholder={this.t('sentence.eg-ttl-in-hours')}
                onChange={(e) => this.handleInputChange(e, "timeout")}
              />
              <Button
                loading={isSet}
                size="large"
                type="primary"
                variant="outlined"
                onClick={() => this.changeConfig("timeout")}
              >
                {change} 
              </Button>
            </Space.Compact>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export default withTranslation()(ConfigForm);