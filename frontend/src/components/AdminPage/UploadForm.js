import React, {useState} from "react";
import { withTranslation } from 'react-i18next';
import {
    message, Upload
} from "antd";
import { ToTopOutlined } from '@ant-design/icons'
const { Dragger } = Upload;

const apiUpload = "/api/v2/superinspire/upload"
const props = {
  name: 'file',
  multiple: true,
  action: apiUpload,
  onChange(info) {
    const { status } = info.file;
    if (status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (status === 'done') {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
  onDrop(e) {
    console.log('Dropped files', e.dataTransfer.files);
  },
};
const UploadForm = () => (
  <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
    <div style={{ width: '50%' }}>
    <Dragger {...props}>
      <p className="ant-upload-drag-icon">
        <ToTopOutlined />
      </p>
      <p className="ant-upload-text">Загрузка файлов</p>
    </Dragger>
    </div>
  </div>
);

export default UploadForm