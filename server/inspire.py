#!/bin/python

import re
import os
import sys
import json
import time

import docker
from flask_cors import CORS
from flask import Flask, Response, request
from werkzeug.utils import secure_filename

from api.bashtesterManager import BashtesterManager

HOUR = 3600                                # Секунд в часе
DEFAULT_MEM = '512'                       # Память по умолчанию (если не задана в конфиге)
DEFAULT_CPU = 1                            # Число ядер по умолчанию (если не задано в конфиге)
DEFAULT_TIMEOUT = 24 * HOUR                # Время работы по умолчанию (если не задано в конфиге)
DEFAULT_IMAGE = "bashtester/ubuntu:latest" # ОС по умолчанию (если не задана в конфиге)
CONFIG_FILE = './config'                   # Расположение config-файла
UPLOAD_FOLDER = '/tests'                   # папка для сохранения загруженных файлов
ALLOWED_EXTENSIONS = {'txt', 'py'}         # расширения файлов, которые разрешено загружать


def read_config(filename=CONFIG_FILE):
    """Функция считывания конфигурационных данных"""
    config = dict()
    try:
        with open(filename, 'r') as file:
            for line in file:
                line = re.sub(r'#.*', '', line, re.S | re.M)
                if re.search('=', line):
                    parameter, value = [x.strip(" \n") for x in line.split('=', maxsplit=1)]
                    if parameter or value:
                        config[parameter] = value
        return config
    except FileNotFoundError:
        sys.exit("Erorr: Файл конфигурации не найден: проверьте ./config")


def check_image(image_name):
    """Проверка наличия образа в локальном регистре Docker"""
    try:
        bashtester_manager.client.images.get(image_name)
    except docker.errors.ImageNotFound:
        return False
    return True


def check_ext(filename: str):
    """ Функция проверки расширения файла """
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


app = Flask(__name__)
CORS(app, resources=r'/*')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

bashtester_manager = BashtesterManager()


@app.route('/v2/superinspire/getOSList')
def return_list():
    """Получение списка доступных систем"""
    response = Response(
        json.dumps(bashtester_manager.OS_LIST), mimetype='application/json')

    response.headers.add('Server', 'python flask')
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET,POST'
    response.headers['Access-Control-Allow-Headers'] = 'x-requested-with'
    return response


@app.route('/v2/superinspire/rmOS')
def rm_OS():
    "Удаление контейнера по имени"
    try:
        container_id = request.args.get('containerId')
    except Exception:
        response = Response(
            json.dumps({
                'message': 'Arguments ERROR',
                'statusCode': 0
            }),
            mimetype='application/json')
    else:
        try:
            isSuccess = bashtester_manager.is_rm_container(container_id)
            if not isSuccess:
                raise Exception

        except Exception as err:
            response = Response(
                json.dumps({
                    'message': f'RM docker containers ERROR: {err}',
                    'shareUrl': '',
                    'statusCode': 0,
                }),
                mimetype='application/json')
        else:
            response = Response(
                json.dumps({
                    'message': 'SUCCESS',
                    'statusCode': 1,
                    'containerId': container_id,
                }),
                mimetype='application/json')

    response.headers.add('Server', 'python flask')
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET,POST'
    response.headers['Access-Control-Allow-Headers'] = 'x-requested-with'
    return response


# Для запуска без возможности выбора ресурсов
@app.route('/v2/superinspire/getOS')
def get_OS():
    """Создание контейнера для пользователя"""
    CONFIG = read_config()  # Считывание конфигурации из ./config
    image_name = CONFIG.get('os')
    if not check_image(image_name):
        response = Response(
            json.dumps({
                'message': 'Error: The image is not supported at this time',
                'statusCode': 0
            }),
            mimetype='application/json')
    else:
        os_cpu = CONFIG.get('cpu')
        os_memory = CONFIG.get('memory')#+'m'
        os_timeout = CONFIG.get('timeout') * HOUR
        container_name = request.args.get('username')
        if os_memory is None:
            os_memory = DEFAULT_MEM
        if os_cpu is None:
            os_cpu = DEFAULT_CPU
        max_timeout = time.time() + DEFAULT_TIMEOUT
        if os_timeout is None:
            os_timeout = max_timeout
        else:
            os_timeout = min(float(os_timeout), max_timeout)

        try:
            container_id = bashtester_manager.is_create_container(
                container_name=container_name,
                os_name=image_name,
                mem=os_memory,
                cpu=os_cpu,
                os_timeout=os_timeout
            )
            if container_id is None:
                raise Exception

        except Exception as err:
            response = Response(
                json.dumps({
                    'message': f'Error: RUN docker containers: {err}',
                    'shareUrl': '',
                    'statusCode': 0,
                }),
                mimetype='application/json')
        else:
            response = Response(
                json.dumps({
                    'message': 'SUCCESS',
                    'shareUrl': f'/console/{container_id}',
                    'statusCode': 1,
                    'containerId': container_id,
                    'timeout': os_timeout
                }),
                mimetype='application/json')
    response.headers.add('Server', 'python flask')
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET,POST'
    response.headers['Access-Control-Allow-Headers'] = 'x-requested-with'
    return response


@app.route('/v2/superinspire/prune')
def pruneTimedoutOS():
    try:
        bashtester_manager.remove_timeout_containers()
        response = Response(
            json.dumps({
                'message': 'Success',
                'statusCode': 1
            }),
            mimetype='application/json')
    except Exception:
        response = Response(
            json.dumps({
                'message': 'ERROR',
                'statusCode': 0
            }),
            mimetype='application/json')

    response.headers.add('Server', 'python flask')
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET,POST'
    response.headers['Access-Control-Allow-Headers'] = 'x-requested-with'
    return response


@app.route('/v2/superinspire/upload', methods=['POST'])
def upload_file():
    files = request.files.getlist("file")
    for file in files:
        if file and check_ext(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    
    response = Response(
            json.dumps({
                'message': 'Success',
                'statusCode': 1
            }),
            mimetype='application/json')

    response.headers.add('Server', 'python flask')
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET,POST'
    response.headers['Access-Control-Allow-Headers'] = 'x-requested-with'
    return response


@app.route('/v2/superinspire/setconfig', methods=['GET'])
def set_config():
    """Функция замены конфигурационных параметров"""
    parameter = request.args.get('param')
    value = request.args.get('value')

    with open(CONFIG_FILE, "r") as file:
        file_content = file.read()

    if parameter == "os":
        replace_text = re.sub(f"^os=.*", f"os={value}", file_content, re.M)
    else:
        replace_text = re.sub(f"{parameter}=\d+", f"{parameter}={value}", file_content)

    with open(CONFIG_FILE, "w") as file:
        file.write(replace_text)
    
    response = Response(
            json.dumps({
                'message': 'Success',
                'statusCode': 1
            }),
            mimetype='application/json')

    response.headers.add('Server', 'python flask')
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET,POST'
    response.headers['Access-Control-Allow-Headers'] = 'x-requested-with'
    return response

'''# Для запуска с возможностью выбора ресурсов
# @app.route('/v2/superinspire/getOS')
# def getOS():
#     open_port = None

#     try:
#         os_name = request.args.get('os')
#         if not instantboxManager.is_os_available(os_name):
#             raise Exception
#     except Exception:
#         response = Response(
#             json.dumps({
#                 'message': 'The image is not supported at this time ERROR',
#                 'statusCode': 0
#             }),
#             mimetype='application/json')
#     else:
#         os_mem = request.args.get('mem')
#         os_cpu = request.args.get('cpu')
#         os_port = request.args.get('port')
#         os_timeout = request.args.get('timeout')

#         if os_mem is None:
#             os_mem = 512
#         if os_cpu is None:
#             os_cpu = 1
#         max_timeout = 3600 * 24 + time.time()
#         if os_timeout is None:
#             os_timeout = max_timeout
#         else:
#             os_timeout = min(float(os_timeout), max_timeout)

#         try:
#             container_name = instantboxManager.is_create_container(
#                 mem=os_mem,
#                 cpu=os_cpu,
#                 os_name=os_name,
#                 open_port=os_port,
#                 os_timeout=os_timeout,
#             )

#             if container_name is None:
#                 raise Exception
#             else:
#                 open_port = "1588"
#                 # ports = instantboxManager.get_container_ports(container_name)
#                 # if os_port is not None:
#                 #     open_port = ports['{}/tcp'.format(os_port)]

#         except Exception as err:
#             print(f'{err} !!!')
#             response = Response(
#                 json.dumps({
#                     'message': 'RUN docker containers ERROR',
#                     'shareUrl': '',
#                     'statusCode': 0,
#                 }),
#                 mimetype='application/json')
#         else:
#             response = Response(
#                 json.dumps({
#                     'message': 'SUCCESS',
#                     'shareUrl': '/console/{}'.format(container_name),
#                     'openPort': open_port,
#                     'statusCode': 1,
#                     'containerId': container_name,
#                 }),
#                 mimetype='application/json')

#     response.headers.add('Server', 'python flask')
#     response.headers['Access-Control-Allow-Origin'] = '*'
#     response.headers['Access-Control-Allow-Methods'] = 'GET,POST'
#     response.headers['Access-Control-Allow-Headers'] = 'x-requested-with'
#     return response
'''

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=int(65501), debug=False)
