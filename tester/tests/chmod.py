import pytest
import difflib

# В начале каждого теста указывается две записи,
# необходимые для системы тестирования:
# 1. record_property("test_desc", "Проверка соединения с контейнером")
# 2. record_property("only_prepod", 1)
# "test_desc" определяет название теста "Проверка соединения с контейнером"
# "only_prepod" помечает тест для преподавателя, если установлена "1",
# а при "0" - доступный для всех

def test_connection(host, record_property):
    """Проверка соединения с контейнером"""
    
    record_property("test_desc", "Проверка соединения с контейнером")
    record_property("only_prepod", 1)
    assert host.run('true').succeeded, "Ошибка соединения с системой"

# Вызывается один раз за сессию тестирования
# и возвращает своё значение в зависимые
# функции-тесты 
@pytest.fixture
def get_new_user_line(host):
    """
        Сравнение нового и старого файлов passwd
        для выявления новых записей о пользователе
    """
    # Чтение содержимого начального файла passwd
    with open("/tests/passwd.txt", "r") as f:
        before = [line.strip() for line in f.readlines()]
    # Чтение содержимого файла passwd в контейнере пользователя
    after = host.file("/etc/passwd").content_string.split("\n")
    
    # Список проверяемых условий: 
    # 1 - пользователь добавлен
    # 2 - у него установлен пароль                             
    check_list = [False, False]
    # Сравнение содержимого 2ух файлов
    diff = difflib.unified_diff(before, 
                                after, 
                                lineterm='', 
                                fromfile='old_file', 
                                tofile='new_file')
    for line in diff:
        # Если строка добавлена, 
        # ей предшествует идентификатор '+'
        if line.startswith('+'):
            # Проверка наличия домашнего каталога
            if ':/home/' in line:
                check_list[0] = True
                # Извлечение имени пользователя из строки
                # напр, из "+user:......"
                user_name = line.split(":")[0].lstrip("+")
                # Получение пароля пользователя user_name
                password = host.user(user_name).password
                # Проверка установки пароля 
                if password is not None and password != "!":
                    check_list[1] = True
    return check_list

@pytest.mark.dependency(name='add_user')
def test_add_user(get_new_user_line, record_property):
    record_property("test_desc", "Создание пользователя")
    record_property("only_prepod", 0)
    # Проверка на то, что пользователь добавлен
    assert get_new_user_line[0] == True, "Пользователь не добавлен"

# Выполняется, только если прошёл test_add_user 
@pytest.mark.dependency(name="add_pass", depends=["add_user"])
def test_add_pass(get_new_user_line,record_property):
    record_property("test_desc", "Установка пароля")
    record_property("only_prepod", 0)
    # Проверка на то что у пользователя установлен пароль
    assert get_new_user_line[1] == True, "У пользователя не установлен пароль"
            
