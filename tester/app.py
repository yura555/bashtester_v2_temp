import json
from os import path
from datetime import date, datetime
from subprocess import run, DEVNULL

import xmltodict
from fastapi import FastAPI, responses

TIME_FORMAT = "%H:%M:%S" # формат времени 
RESULTS_FOLDER = "/results" # каталог результатов
TESTS_FOLDER = "/tests" # каталог тестов
    

def get_info_tests(data):
    """Извлечение информации от тестах"""
    user_results = list()
    prepod_results = list()

    tests_info = data['testsuites']['testsuite']
    if tests_info['failures'] == "0" and \
       tests_info['errors'] == "0" and \
       tests_info['skipped'] == "0":

        user_results.append({'name': 'Все тесты', 
                             'result': 'прошли'})
        prepod_results.append({'name': 'Все тесты', 
                               'result': 'прошли'})
    else:
        for test in tests_info['testcase']:
            # Если пропущен, записывается только преподавателю
            if test.get('skipped'):
                name = test['name'].split("[")[0]
                prepod_results.append({'name': name, 
                                       'result': "пропущен"})
                continue
            properties = test['properties']['property']
            # Название теста
            name = [prop['value'] for prop in properties \
                    if prop['name'] == 'test_desc'][0]
            # Проверка установки: тест только для преподавателя
            for_prepod = [prop['value'] for prop in properties \
                          if prop['name'] == 'only_prepod'][0]

            if test.get('failure'):
                # Извлечение описания с отбрасыванием типа ошибки
                err_desc = (test['failure']['message']).split(":")[1]
                # Тест только для преподавателя
                if bool(int(for_prepod)):
                    prepod_results.append({'name': name, 'result': err_desc})
                    continue
                prepod_results.append({'name': name, 'result': err_desc})
                user_results.append({'name': name, 'result': err_desc})
            # Если ошибка выполнения, записывается только преподавателю
            elif test.get('error'):
                prepod_results.append({'name': name, 
                                       'result': f"{test['error']['message']}"})
            else:
                # Тест только для преподавателя
                if bool(int(for_prepod)):
                    prepod_results.append({'name': name, 'result': 'прошел'})
                    continue
                prepod_results.append({'name': name, 'result': 'прошел'})
                user_results.append({'name': name, 'result': 'прошел'})
    return user_results, prepod_results


app = FastAPI()


@app.get("/tester")
async def tester(containerId: str, testName: str):
    """
        Программа проверки
    """
    headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET,POST',
        'Access-Control-Allow-Headers': 'x-requested-with'
    }
    try:
        test_path = f"{TESTS_FOLDER}/{testName}.py"
        if not path.isfile(test_path):
            print("File do not exist")
            message = {'message': f"Test file '{testName}' do not exist",
                    'tests': '',
                    'statusCode': 0}
            return responses.JSONResponse(content=message, headers=headers)

        host = f"--hosts=ssh://{containerId}"
        day = date.today()
        now = datetime.now().strftime(TIME_FORMAT)
        result_file = f"{RESULTS_FOLDER}/{containerId}/{day}/{now}-{testName}"
        command = f"py.test --tb=no --assert=plain --junitxml={result_file} {host} {test_path}"
        run(command, shell=True, stderr=DEVNULL)

        with open(result_file, 'r') as file:
            xml_data = file.read()
            data_dict = xmltodict.parse(xml_data, attr_prefix='')    

        user_results, prepod_results = get_info_tests(data_dict)

        with open(result_file, 'w') as file:
            json_data = json.dumps(prepod_results, indent=4)
            file.write(json_data)
    except Exception as err: 
        message = {'message': f"Error: fail testing {err}",
                    'tests': '',
                    'statusCode': 0}
        return responses.JSONResponse(content=message, headers=headers)

        
    response_data = {'tests': user_results, 
                    'statusCode': 1, 
                    'message': 'SUCCESS'}

    return responses.JSONResponse(content=response_data, headers=headers)
